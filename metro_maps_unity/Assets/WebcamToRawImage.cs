﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebcamToRawImage : MonoBehaviour 
    {
        public RawImage rawimage;
        public RectTransform panelContainer;
        void Update()
        {
            WebCamTexture webcamTexture = new WebCamTexture();
            rawimage.texture = webcamTexture;
            rawimage.material.mainTexture = webcamTexture;
            webcamTexture.Play();
            print("W: " + rawimage.texture.width + "H:" + rawimage.texture.height);
            float ratio = (float)Screen.height / (float)Screen.width ;
            panelContainer.sizeDelta = new Vector2( rawimage.texture.width * ratio, 0);
        }
    }