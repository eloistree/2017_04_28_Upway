﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OriantateWithCompas : MonoBehaviour {

          public Text _debug;
    public float _lastCompass;
    public float _currentCompass;
    public float _lerpRatio=1;

    [Header("Debug")]
    [Range(0f, 360f)]
    public float _horizontalDebug;
    [Range(-1f,1f)]
    public float _verticalDebug;

    private void Awake()
    {
        Input.compass.enabled = true;
    }
    void Update()
        {

            float horizontalRotation=0;
            _currentCompass=  _lastCompass = Input.compass.magneticHeading;
           // _currentCompass = Mathf.Lerp(_currentCompass, _lastCompass, Time.deltaTime * _lerpRatio);
            _debug.text = "Compass:" + _currentCompass +" Acceleromerter"+ Input.acceleration;
            horizontalRotation = _currentCompass;


            float verticalRotation = Input.acceleration.z * -90f;
//#if UNITY_EDITOR
        horizontalRotation= _horizontalDebug;
        verticalRotation = _verticalDebug *-90f;
//#endif



        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, horizontalRotation, 0) * Quaternion.Euler(verticalRotation, 0, 0), Time.deltaTime*_lerpRatio);
        }
    }

