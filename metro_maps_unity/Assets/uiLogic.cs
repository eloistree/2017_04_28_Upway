﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiLogic : MonoBehaviour
{
	public GameObject speedControls;
	public Toggle uiToggle;
	public Slider uiSlider;

	public void Start()
	{
	}

	public void Update()
	{
	}

	public void ToggleButtonPressed()
	{
		Debug.Log("useRealtime");
		speedControls.SetActive(!uiToggle.isOn);
		ManageRealtimeMetros.inst.useRealtime = uiToggle.isOn;
	}

	public void UpdateWaitSpeed()
	{
		ManageRealtimeMetros.waitTime = uiSlider.value;
	}
}
