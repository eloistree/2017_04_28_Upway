﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtMainCamera : MonoBehaviour {

    void Update()
    {
        Camera camera = Camera.main;
        if (camera)
        {
            this.transform.LookAt(camera.transform.position);
            transform.Rotate(new Vector3(0,180,0));
        }

    }
}
