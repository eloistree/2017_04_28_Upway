﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class GPSViewer : MonoBehaviour
{

	[SerializeField]
	public GPSTarget _currentPosition;

	public Transform _direction;
	public double _ratioDegreeToMeter = 40080 / 360.0;
	public double _ratioMeterByUnit = 100;
	[Range(0, 1f)]
	public double _scaleFactor = 1f;
	[SerializeField]
	public List<GPSTarget> _managedTargets;


	void Start()
	{
		FindTargets();
	}

	void Update()
	{
		for (int i = 0; i < _managedTargets.Count; i++)
		{
			Vector3 pos = GetLocalPosition(_currentPosition, _managedTargets[i]);
			_managedTargets[i].transform.position = pos;
		}
	}
	public void FindTargets()
	{
		_managedTargets.Clear();
		_managedTargets.AddRange(GameObject.FindObjectsOfType<GPSTarget>());
	}

	public Vector3 GetLocalPosition(GPSTarget root, GPSTarget toPositionate)
	{

		GPSPosition posDirection = toPositionate.Position - root.Position;
		double ratio = (_ratioMeterByUnit == 0 ? _ratioDegreeToMeter : _ratioDegreeToMeter / _ratioMeterByUnit) * _scaleFactor;
		return new Vector3((float)(posDirection.Longitude * ratio), 0, (float)(posDirection.Latitude * ratio));
	}

}
