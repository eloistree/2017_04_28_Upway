﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnityToGPSTarget : MonoBehaviour {

    [SerializeField]
    public GPSTarget _affectedTarget;


    [Header("Debug")]
    public bool _valueRetreived;

    IEnumerator Start()
    {
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
            yield break;

        // Start service before querying location
        Input.location.Start();

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            yield break;
        }
        else
        {
            _valueRetreived = true;
            _affectedTarget.SetPosition(Input.location.lastData.latitude, Input.location.lastData.longitude, Input.location.lastData.altitude);
            yield return new WaitForSeconds(1f);

            // print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
        }

    }

    public void OnDestroy()
    {


        Input.location.Stop();
    }
}
