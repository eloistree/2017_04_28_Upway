﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPSTarget : MonoBehaviour ,IHasGpsPosition {


    [SerializeField]
    private GPSPosition _gpsPosition;

    public GPSPosition Position
    {
        get { return _gpsPosition; }
        set { _gpsPosition = value; }
    }

    public GPSPosition GetPosition()
    {
        return Position;
    }

    public void SetPosition(GPSPosition gpsPosition)
    {
        _gpsPosition.SetWith( gpsPosition);
    }

    public void SetPosition(double longitude, double latitude, double altitude)
    {
        _gpsPosition.SetWith(longitude, latitude, altitude);
    }
}
