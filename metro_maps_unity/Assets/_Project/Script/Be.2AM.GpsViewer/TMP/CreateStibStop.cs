﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateStibStop : MonoBehaviour
{

	public static List<MetroDataStop> metroStops;
    public GameObject _prefabMetroStop;

	public static GPSPosition GetGpsForHalte(string stopName)
	{
		stopName = stopName.ToUpper();
		var stop = metroStops.Find(x => x.name == stopName);
		if (stop != null) return stop.gpsPos;
		stop = metroStops.Find(x => x.id == stopName);
		if (stop != null) return stop.gpsPos;

		throw new Exception("Stop not found: "+stopName);
	}

	void Awake ()
	{
		metroStops = STIB_FileAccess.LoadMetroStopCollection("stops");
		var haltes = ManageRealtimeMetros.GetHaltesOnTrips();

		foreach (MetroDataStop metroStop in metroStops)
		{
			if (metroStop == null || metroStop.name == null) continue;
			if (!haltes.Contains(metroStop.name.ToUpper())) continue;
			GameObject gamo = Instantiate(_prefabMetroStop);
			gamo.name = metroStop.name + ": " + metroStop.position.z + " , " + metroStop.position.x;
			GPSTarget gps = gamo.GetComponent<GPSTarget>();
			gps.SetPosition(metroStop.position.z, metroStop.position.x, 0);
		}
	}
	
}
