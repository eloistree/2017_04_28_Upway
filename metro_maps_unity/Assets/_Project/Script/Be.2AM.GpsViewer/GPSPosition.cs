﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface IHasGpsPosition
{

    void SetPosition(GPSPosition gpsPosition);
    GPSPosition GetPosition();
    void SetPosition(double longitude, double latitude, double altitude);
}
[System.Serializable]
public class GPSPosition {

    [SerializeField]
    [Tooltip("Degree from -180 to 180")]
    private double _latitude;
    public double Latitude { get { return _latitude; } }
    [SerializeField]
    [Tooltip("Degrre from -90 to 90")]
    private double _longitude;
    public double Longitude { get { return _longitude; } }
    [SerializeField]
    [Tooltip("Altitude in MM from the sea")]
    private double _altitude;
    public double Altitude { get { return _altitude; } }

    public void SetWith(GPSPosition gpsPosition)
    {
        _longitude = gpsPosition._longitude;
        _latitude = gpsPosition._latitude;
        _altitude = gpsPosition._altitude;
        
    }

    public void SetWith(double longitude, double latitude, double altitude)
    {
        _longitude = longitude;
        _latitude = latitude;
        _altitude = altitude;
    }

    public static GPSPosition operator +(GPSPosition b, GPSPosition c)
    {
        GPSPosition value = new GPSPosition();
        value._longitude = b._longitude + c._longitude;
        value._latitude = b._latitude + c._latitude;
        value._altitude = b._altitude + c._altitude;
        return value;
    }
    public static GPSPosition operator -(GPSPosition b, GPSPosition c)
    {
        GPSPosition value = new GPSPosition();
        value._longitude = b._longitude - c._longitude;
        value._latitude = b._latitude - c._latitude;
        value._altitude = b._altitude - c._altitude;
        return value;
    }


}



