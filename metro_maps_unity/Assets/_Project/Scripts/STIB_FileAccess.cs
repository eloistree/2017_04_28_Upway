﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class STIB_FileAccess : MonoBehaviour
{

	public static List<MetroDataStop> LoadMetroStopCollection(string fileName = "stops")
	{
		var grid = CsvFileToArray(fileName);
		return ConvertCsvToMetroStop(grid);
	}

	private static List<MetroDataStop> ConvertCsvToMetroStop(string[,] grid)
	{
		List<MetroDataStop> result = new List<MetroDataStop>();

		//    0      1         2         3         4       5        6       7          8      9
		//stop_id,stop_code,stop_name,stop_desc,stop_lat,stop_lon,zone_id,stop_url,location_type
		for (int i = 0; i < grid.GetLength(1); i++)
		{
			float stop_lat = 0; float.TryParse(grid[4, i], out stop_lat);
			float stop_lon = 0; float.TryParse(grid[5, i], out stop_lon);
			result.Add(new MetroDataStop(grid[0, i], grid[2, i], new Vector3(stop_lat, 0, stop_lon)));
		}
		return result;
	}

	public static string[,] CsvFileToArray(string fileName)
	{
		TextAsset txtAsset = (TextAsset)Resources.Load(fileName);
		string csvText = txtAsset.text;

		var grid = CSVReader.SplitCsvGrid(csvText);
		return grid;
	}

	public static MetroDataModel LoadMetroDataModel()
	{
		CSVReader.maxRows = 999999;
		var metroDta = new MetroDataModel();
		metroDta.metroStops = LoadMetroStopCollection();


		/*{
			var grid = CsvFileToArray("routes");
			metroDta.metroRoutes.Clear();
			for (int i = 0; i < grid.GetLength(1); i++)
			{
				metroDta.metroRoutes.Add(new MetroDataRoute(grid[0, i], grid[1, i], grid[2, i], grid[3, i], grid[4, i], grid[6, i], grid[7, i]));
			}
		}*/

		CSVReader.maxRows = 10000;
		/*
		{
			var grid = CsvFileToArray("shapes");
			metroDta.metroShapes.Clear();
			for (int i = 0; i < grid.GetLength(1); i++)
			{
				float shape_pt_lat = 0; float.TryParse(grid[1, i], out shape_pt_lat);
				float shape_pt_lon = 0; float.TryParse(grid[2, i], out shape_pt_lon);
				metroDta.metroShapes.Add(new MetroDataShape(grid[0, i], shape_pt_lat, shape_pt_lon));
			}
		}
		*/
		return metroDta; // Don't parse the folwing files. Too heavy

		/*

		{
			var grid = CsvFileToArray("trips");
			metroDta.metroTrips.Clear();
			for (int i = 0; i < grid.GetLength(1); i++)
			{
				var route = metroDta.metroRoutes.Find(x => x.route_id == grid[0, i]);
				var shape = metroDta.metroShapes.Find(x => x.shape_id == grid[6, i]);
				metroDta.metroTrips.Add(new MetroDataTrip(grid[2, i], route, shape));
			}
		}

		{
			var grid = CsvFileToArray("stop_times");
			metroDta.metroStopsTimes.Clear();
			//     0         1           2             3         4             5          6
			// trip_id,arrival_time,departure_time,stop_id,stop_sequence,pickup_type,drop_off_type
			for (int i = 0; i < grid.GetLength(1); i++)
			{
				var trip = metroDta.metroTrips.Find(x => x.trip_id == grid[0, i]);
				var stop = metroDta.metroStops.Find(x => x.id == grid[3, i]);
				metroDta.metroStopsTimes.Add(new MetroDataStopTime(trip, stop));
			}
		}

		return metroDta;*/
	}

}
