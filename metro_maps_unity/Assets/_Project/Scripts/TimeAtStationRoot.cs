﻿
using System;
using System.Collections.Generic;

[System.Serializable]
public class VehiclePosition
{
	public string directionId;
	public string distanceFromPoint;
	public double @long;
	public double lat;
	public string station;

	public override bool Equals(object obj)
	{
		if (obj == null || GetType() != obj.GetType())
			return false;

		VehiclePosition p = (VehiclePosition)obj;
		const double sigma = 0.00001;
		return
			this.directionId == p.directionId &&
			this.distanceFromPoint == p.distanceFromPoint &&
			Math.Abs(this.lat - p.lat) < sigma &&
			Math.Abs(this.@long - p.@long) < sigma &&
			this.station == p.station;
	}

	public override int GetHashCode()
	{

		return directionId.GetHashCode() + distanceFromPoint.GetHashCode() + @long.GetHashCode() + lat.GetHashCode() +
		       station.GetHashCode();
	}
}

[System.Serializable]
public class Line
{
	public int lineId;
	public List<VehiclePosition> vehiclePositions;
}

[System.Serializable]
public class TimeAtStationRoot
{
	public List<Line> lines;
}
