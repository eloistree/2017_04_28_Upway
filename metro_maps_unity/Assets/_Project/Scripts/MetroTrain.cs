﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetroTrain : MonoBehaviour
{
	private GPSPosition targetPos;

	private VehiclePosition _vehiclePosition;

	public VehiclePosition vehiclePosition
	{
		get { return _vehiclePosition; }
		set
		{
			if (_vehiclePosition != null && _vehiclePosition.Equals(value)) return;

			if (value != null)
			{
				if (_vehiclePosition == null)
					_gpsTarget.Position.SetWith(value.@long, value.lat, 0);
				targetPos = CreateStibStop.GetGpsForHalte(value.station); //new GPSPosition();
				targetPos.SetWith(value.@long, value.lat, 0);
			}
			_vehiclePosition = value;
		}
	}

	private GPSTarget _gpsTarget;
	private TrailRenderer trainRenderer;

	public void Awake()
	{
		_gpsTarget = GetComponent<GPSTarget>();
		trainRenderer = GetComponent<TrailRenderer>();
	}

	public void Start()
	{
	}

	public void Update()
	{
		if (transform.position.sqrMagnitude > 1)
		{
			trainRenderer.enabled = true;
		}
		if (targetPos != null)
		{
			double alpha = 0.3;
			var lat = _gpsTarget.Position.Latitude * (1 - alpha) + targetPos.Latitude * alpha;
			var lon = _gpsTarget.Position.Longitude * (1 - alpha) + targetPos.Longitude * alpha;
			_gpsTarget.Position.SetWith(lon, lat, 0);
		}

		if (vehiclePosition == null) return;
	}
}
