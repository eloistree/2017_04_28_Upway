﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Permissions;
using UnityEngine;
using Random = UnityEngine.Random;

public class MetroDataTrip
{
	/// <summary>
	///     0          1         2        3             4           5      6
	/// route_id,service_id,trip_id,trip_headsign,direction_id,block_id,shape_id
	/// </summary>
	public MetroDataTrip(string trip_id, MetroDataRoute route, MetroDataShape shape)
	{
		this.trip_id = trip_id;
		this.route = route;
		this.shape = shape;
	}

	public readonly string trip_id;
	public readonly MetroDataRoute route;
	public readonly MetroDataShape shape;
}

public class MetroDataRoute
{
	/// <summary>
	///     0         1                   2             3         4         5          6             7
	/// route_id,route_short_name,route_long_name,route_desc,route_type,route_url,route_color,route_text_color
	/// </summary>
	public MetroDataRoute(string route_id, string route_short_name, string route_long_name, string route_desc, string route_type, string route_color, string route_text_color)
	{
		this.route_id = route_id;
		this.route_short_name = route_short_name;
		this.route_long_name = route_long_name;
		this.route_desc = route_desc;
		this.route_type = route_type;
		this.route_color = route_color;
		this.route_text_color = route_text_color;
	}

	public readonly string route_id;
	public readonly string route_short_name;
	public readonly string route_long_name;
	public readonly string route_desc;
	public readonly string route_type;
	public readonly string route_color;
	public readonly string route_text_color;
}


public class MetroDataStopTime
{
	/// <summary>
	///     0          1            2           3         4              5        6
	/// trip_id,arrival_time,departure_time,stop_id,stop_sequence,pickup_type,drop_off_type
	/// </summary>
	/// <param name="trip"></param>
	/// <param name="stop"></param>
	public MetroDataStopTime(MetroDataTrip trip, MetroDataStop stop)
	{
		this.trip = trip;
		this.stop = stop;
	}

	public readonly MetroDataTrip trip;
	public readonly MetroDataStop stop;
}

public class MetroDataStopConnection
{
	public MetroDataStopConnection(MetroDataStop stopA, MetroDataStop stopB)
	{
		this.stopA = stopA;
		this.stopB = stopB;
	}

	public readonly MetroDataStop stopA;
	public readonly MetroDataStop stopB;
}

public class MetroDataStop
{
	public MetroDataStop(string id, string name, Vector3 pos)
	{
		this.id = id;
		this.name = name;
		this.position = pos;
		this.gpsPos = new GPSPosition();
		gpsPos.SetWith(pos.x, pos.z, 0);
	}

	public readonly string id;
	public readonly string name;
	public readonly Vector3 position;
	public readonly GPSPosition gpsPos;

	public override string ToString()
	{
		return "(" + id + ") " + name;
	}
}

public class MetroDataShape
{
	/// <summary>
	/// shape_id,shape_pt_lat,shape_pt_lon,shape_pt_sequence
	/// </summary>
	public MetroDataShape(string shape_id, float shape_pt_lat, float shape_pt_lon)
	{
		this.shape_id = shape_id;
		this.shape_pt_lat = shape_pt_lat;
		this.shape_pt_lon = shape_pt_lon;
		this.position = new Vector3(shape_pt_lat, 0, shape_pt_lon);
	}

	public readonly string shape_id;
	public readonly float shape_pt_lat;
	public readonly float shape_pt_lon;
	public readonly Vector3 position;
	public readonly string shape_pt_sequence;
}
public class MetroDataCalender
{
	/// <summary>
	/// service_id,monday,tuesday,wednesday,thursday,friday,saturday,sunday,start_date,end_date
	/// </summary>
	public MetroDataCalender(string service_id, int start_date, int end_date)
	{
		this.service_id = service_id;
		this.start_date = start_date;
		this.end_date = end_date;
	}

	public string service_id;
	public int monday;
	public int tuesday;
	public int wednesday;
	public int thursday;
	public int friday;
	public int saturday;
	public int sunday;
	public int start_date;
	public int end_date;
}

public class MetroDataTrain
{
	public MetroDataTrain(MetroDataStopConnection connection, float percentage)
	{
		this.connection = connection;
		this.percentage = percentage;
		this.percentagePerSecond = 0.5f; //Random.Range(-2, 2);
	}

	public Vector3 position
	{
		get { return Vector3.Lerp(connection.stopA.position, connection.stopB.position, percentage); }
	}
	public MetroDataStopConnection connection;
	public float percentage;
	public float percentagePerSecond;
}

public class MetroDataModel
{
	[Obsolete("Derived data")]
	public List<MetroDataStopConnection> metroConnections = new List<MetroDataStopConnection>();
	[Obsolete("Derived data")]
	public List<MetroDataTrain> metroTrains = new List<MetroDataTrain>();

	public List<MetroDataStop> metroStops = new List<MetroDataStop>();
	public List<MetroDataStopTime> metroStopsTimes = new List<MetroDataStopTime>();
	public List<MetroDataTrip> metroTrips = new List<MetroDataTrip>();
	public List<MetroDataRoute> metroRoutes = new List<MetroDataRoute>();
	public List<MetroDataShape> metroShapes = new List<MetroDataShape>();
	public List<MetroDataCalender> metroCalendars = new List<MetroDataCalender>();
}

public class MetroDispatch : MonoBehaviour
{
	public GameObject shape_marker;
	public GameObject metro_stop;
	public GameObject metro_train;

	public bool reloadBtn = true;

	public static MetroDispatch inst;
	public void Start()
	{
		inst = this;
		this.dataModel = STIB_FileAccess.LoadMetroDataModel();
	}

	/// <summary>
	/// Call this to update
	/// </summary>
	public void ReactMetroHaltes()
	{
		if (dataModel == null) return;

		Debug.Log("ReactMetroHaltes");

		foreach (Transform child in transform)
		{
			DestroyImmediate(child.gameObject);
		}

		foreach (var stop in dataModel.metroStops)
		{
			var instance = Instantiate(this.metro_stop, stop.position, Quaternion.identity, transform);
			instance.GetComponent<MetroStop>().metroDataStop = stop;
		}

		foreach (var shape in dataModel.metroShapes)
		{
			Instantiate(this.shape_marker, shape.position, Quaternion.identity, transform);
		}

		/*foreach (var train in dataModel.metroTrains)
		{
			var instance = Instantiate(this.metro_train, train.position, Quaternion.identity, transform);
			instance.GetComponent<MetroTrain>().dataTrain = train;
		}*/
	}

	public MetroDataModel dataModel;


	private void UpdateMetroData()
	{
		if (this.dataModel == null) return;

	}

	public void Update()
	{
		if (reloadBtn)
		{
			reloadBtn = false;
			ReactMetroHaltes();
		}

		UpdateMetroData();
	}

	public void OnDrawGizmos()
	{
		if (dataModel == null) return;
	}
}
