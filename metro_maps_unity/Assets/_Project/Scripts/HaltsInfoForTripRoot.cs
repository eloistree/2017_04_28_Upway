﻿using System.Collections.Generic;

[System.Serializable]
public class HaltsInfoForTrip
{
	public string stop_lat;
	public string stop_lon;
	public string stop_name;
	public string stop_sequence;
}

[System.Serializable]
public class HaltsInfoForTripRoot
{
	public List<HaltsInfoForTrip> haltsInfoForTrip;
}