﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ManageRealtimeMetros : MonoBehaviour
{
	public GameObject metro_train;
	public static ManageRealtimeMetros inst;

	public string urlTimeAtStation = "http://192.168.50.59:8080/dataOpened2/TimeAtStation?line=5,6";

	private TimeAtStationRoot _timeAtStation;

	public static readonly Dictionary<int, string> TheoricalLine_Route = new Dictionary<int, string>()
	{
		{ 001, "94809425-M20170306-000m-Semaine-00"},
		{ 002, "94809868-M20170306-000m-Semaine-00"},
		{ 003, "95321083-T20170306-004t-Dimanche-00"},
		{ 004, "95320999-T20170306-004t-Dimanche-00"},
		{ 005, "94809430-M20170306-000m-Semaine-00"},
		{ 006, "94809899-M20170306-000m-Semaine-00"}
	};

	/// <summary>
	/// Cache
	/// </summary>
	public static readonly Dictionary<string, HaltsInfoForTripRoot> tripId_to_Positions = new Dictionary<string, HaltsInfoForTripRoot>()
	{
		{"94809425-M20170306-000m-Semaine-00", JsonUtility.FromJson<HaltsInfoForTripRoot>("{ \"haltsInfoForTrip\" : [{\"stop_lat\":\"50.848609\",\"stop_lon\":\"4.321055\",\"stop_name\":\"GARE DE L\'OUEST\",\"stop_sequence\":\"1\"},{\"stop_lat\":\"50.853302\",\"stop_lon\":\"4.323138\",\"stop_name\":\"BEEKKANT\",\"stop_sequence\":\"2\"},{\"stop_lat\":\"50.857117\",\"stop_lon\":\"4.332564\",\"stop_name\":\"ETANGS NOIRS\",\"stop_sequence\":\"3\"},{\"stop_lat\":\"50.854800\",\"stop_lon\":\"4.340533\",\"stop_name\":\"COMTE DE FLANDRE\",\"stop_sequence\":\"4\"},{\"stop_lat\":\"50.851997\",\"stop_lon\":\"4.347948\",\"stop_name\":\"SAINTE-CATHERINE\",\"stop_sequence\":\"5\"},{\"stop_lat\":\"50.849795\",\"stop_lon\":\"4.352536\",\"stop_name\":\"DE BROUCKERE\",\"stop_sequence\":\"6\"},{\"stop_lat\":\"50.846344\",\"stop_lon\":\"4.358685\",\"stop_name\":\"GARE CENTRALE\",\"stop_sequence\":\"7\"},{\"stop_lat\":\"50.845759\",\"stop_lon\":\"4.362931\",\"stop_name\":\"PARC\",\"stop_sequence\":\"8\"},{\"stop_lat\":\"50.845310\",\"stop_lon\":\"4.369633\",\"stop_name\":\"ARTS-LOI\",\"stop_sequence\":\"9\"},{\"stop_lat\":\"50.843665\",\"stop_lon\":\"4.377612\",\"stop_name\":\"MAELBEEK\",\"stop_sequence\":\"10\"},{\"stop_lat\":\"50.842621\",\"stop_lon\":\"4.382553\",\"stop_name\":\"SCHUMAN\",\"stop_sequence\":\"11\"},{\"stop_lat\":\"50.839382\",\"stop_lon\":\"4.398154\",\"stop_name\":\"MERODE\",\"stop_sequence\":\"12\"},{\"stop_lat\":\"50.837590\",\"stop_lon\":\"4.407977\",\"stop_name\":\"MONTGOMERY\",\"stop_sequence\":\"13\"},{\"stop_lat\":\"50.840519\",\"stop_lon\":\"4.414099\",\"stop_name\":\"JOSEPH.-CHARLOTTE\",\"stop_sequence\":\"14\"},{\"stop_lat\":\"50.842512\",\"stop_lon\":\"4.418829\",\"stop_name\":\"GRIBAUMONT\",\"stop_sequence\":\"15\"},{\"stop_lat\":\"50.844451\",\"stop_lon\":\"4.425973\",\"stop_name\":\"TOMBERG\",\"stop_sequence\":\"16\"},{\"stop_lat\":\"50.847493\",\"stop_lon\":\"4.436129\",\"stop_name\":\"ROODEBEEK\",\"stop_sequence\":\"17\"},{\"stop_lat\":\"50.847513\",\"stop_lon\":\"4.447233\",\"stop_name\":\"VANDERVELDE\",\"stop_sequence\":\"18\"},{\"stop_lat\":\"50.849891\",\"stop_lon\":\"4.453329\",\"stop_name\":\"ALMA\",\"stop_sequence\":\"19\"},{\"stop_lat\":\"50.848484\",\"stop_lon\":\"4.458921\",\"stop_name\":\"CRAINHEM\",\"stop_sequence\":\"20\"},{\"stop_lat\":\"50.841881\",\"stop_lon\":\"4.464645\",\"stop_name\":\"STOCKEL\",\"stop_sequence\":\"21\"}]}")},
		{"94809868-M20170306-000m-Semaine-00", JsonUtility.FromJson<HaltsInfoForTripRoot>("{ \"haltsInfoForTrip\" : [{\"stop_lat\":\"50.844978\",\"stop_lon\":\"4.324452\",\"stop_name\":\"DELACROIX\",\"stop_sequence\":\"1\"},{\"stop_lat\":\"50.840899\",\"stop_lon\":\"4.331214\",\"stop_name\":\"CLEMENCEAU\",\"stop_sequence\":\"2\"},{\"stop_lat\":\"50.836092\",\"stop_lon\":\"4.338259\",\"stop_name\":\"GARE DU MIDI\",\"stop_sequence\":\"3\"},{\"stop_lat\":\"50.833450\",\"stop_lon\":\"4.343030\",\"stop_name\":\"PORTE DE HAL\",\"stop_sequence\":\"4\"},{\"stop_lat\":\"50.832966\",\"stop_lon\":\"4.349049\",\"stop_name\":\"HOTEL DES MONNAIES\",\"stop_sequence\":\"5\"},{\"stop_lat\":\"50.835295\",\"stop_lon\":\"4.354414\",\"stop_name\":\"LOUISE\",\"stop_sequence\":\"6\"},{\"stop_lat\":\"50.838280\",\"stop_lon\":\"4.361569\",\"stop_name\":\"PORTE DE NAMUR\",\"stop_sequence\":\"7\"},{\"stop_lat\":\"50.841238\",\"stop_lon\":\"4.366509\",\"stop_name\":\"TRONE\",\"stop_sequence\":\"8\"},{\"stop_lat\":\"50.845481\",\"stop_lon\":\"4.368710\",\"stop_name\":\"ARTS-LOI\",\"stop_sequence\":\"9\"},{\"stop_lat\":\"50.849895\",\"stop_lon\":\"4.369164\",\"stop_name\":\"MADOU\",\"stop_sequence\":\"10\"},{\"stop_lat\":\"50.853778\",\"stop_lon\":\"4.365344\",\"stop_name\":\"BOTANIQUE\",\"stop_sequence\":\"11\"},{\"stop_lat\":\"50.855531\",\"stop_lon\":\"4.358314\",\"stop_name\":\"ROGIER\",\"stop_sequence\":\"12\"},{\"stop_lat\":\"50.857454\",\"stop_lon\":\"4.351198\",\"stop_name\":\"YSER\",\"stop_sequence\":\"13\"},{\"stop_lat\":\"50.860580\",\"stop_lon\":\"4.339649\",\"stop_name\":\"RIBAUCOURT\",\"stop_sequence\":\"14\"},{\"stop_lat\":\"50.863130\",\"stop_lon\":\"4.330671\",\"stop_name\":\"ELISABETH\",\"stop_sequence\":\"15\"}]}")},
		{"95321083-T20170306-004t-Dimanche-00", JsonUtility.FromJson<HaltsInfoForTripRoot>("{ \"haltsInfoForTrip\" : [{\"stop_lat\":\"50.879398\",\"stop_lon\":\"4.370675\",\"stop_name\":\"DOCKS BRUXSEL\",\"stop_sequence\":\"1\"},{\"stop_lat\":\"50.874544\",\"stop_lon\":\"4.362629\",\"stop_name\":\"MABRU\",\"stop_sequence\":\"2\"},{\"stop_lat\":\"50.871379\",\"stop_lon\":\"4.360925\",\"stop_name\":\"JULES DE TROOZ\",\"stop_sequence\":\"3\"},{\"stop_lat\":\"50.869168\",\"stop_lon\":\"4.362516\",\"stop_name\":\"MASUI\",\"stop_sequence\":\"4\"},{\"stop_lat\":\"50.865312\",\"stop_lon\":\"4.362857\",\"stop_name\":\"THOMAS\",\"stop_sequence\":\"5\"},{\"stop_lat\":\"50.860664\",\"stop_lon\":\"4.359889\",\"stop_name\":\"GARE DU NORD\",\"stop_sequence\":\"6\"}]}")},
		{"95320999-T20170306-004t-Dimanche-00", JsonUtility.FromJson<HaltsInfoForTripRoot>("{ \"haltsInfoForTrip\" : [{\"stop_lat\":\"50.860664\",\"stop_lon\":\"4.359889\",\"stop_name\":\"GARE DU NORD\",\"stop_sequence\":\"1\"},{\"stop_lat\":\"50.855702\",\"stop_lon\":\"4.358030\",\"stop_name\":\"ROGIER\",\"stop_sequence\":\"2\"},{\"stop_lat\":\"50.851179\",\"stop_lon\":\"4.352436\",\"stop_name\":\"DE BROUCKERE\",\"stop_sequence\":\"3\"},{\"stop_lat\":\"50.847601\",\"stop_lon\":\"4.348731\",\"stop_name\":\"BOURSE\",\"stop_sequence\":\"4\"},{\"stop_lat\":\"50.844067\",\"stop_lon\":\"4.345069\",\"stop_name\":\"ANNEESSENS\",\"stop_sequence\":\"5\"},{\"stop_lat\":\"50.840120\",\"stop_lon\":\"4.341011\",\"stop_name\":\"LEMONNIER\",\"stop_sequence\":\"6\"},{\"stop_lat\":\"50.836712\",\"stop_lon\":\"4.337520\",\"stop_name\":\"GARE DU MIDI\",\"stop_sequence\":\"7\"},{\"stop_lat\":\"50.832434\",\"stop_lon\":\"4.342988\",\"stop_name\":\"PORTE DE HAL\",\"stop_sequence\":\"8\"},{\"stop_lat\":\"50.829657\",\"stop_lon\":\"4.346637\",\"stop_name\":\"PARVIS ST-GILLES\",\"stop_sequence\":\"9\"},{\"stop_lat\":\"50.825837\",\"stop_lon\":\"4.345589\",\"stop_name\":\"HORTA\",\"stop_sequence\":\"10\"},{\"stop_lat\":\"50.821171\",\"stop_lon\":\"4.343548\",\"stop_name\":\"ALBERT\",\"stop_sequence\":\"11\"},{\"stop_lat\":\"50.817656\",\"stop_lon\":\"4.346160\",\"stop_name\":\"BERKENDAEL\",\"stop_sequence\":\"12\"},{\"stop_lat\":\"50.813090\",\"stop_lon\":\"4.347581\",\"stop_name\":\"VANDERKINDERE\",\"stop_sequence\":\"13\"},{\"stop_lat\":\"50.809808\",\"stop_lon\":\"4.345455\",\"stop_name\":\"MESSIDOR\",\"stop_sequence\":\"14\"},{\"stop_lat\":\"50.806640\",\"stop_lon\":\"4.343477\",\"stop_name\":\"BOETENDAEL\",\"stop_sequence\":\"15\"},{\"stop_lat\":\"50.803308\",\"stop_lon\":\"4.341358\",\"stop_name\":\"HEROS\",\"stop_sequence\":\"16\"},{\"stop_lat\":\"50.800538\",\"stop_lon\":\"4.337090\",\"stop_name\":\"GLOBE\",\"stop_sequence\":\"17\"},{\"stop_lat\":\"50.796742\",\"stop_lon\":\"4.329050\",\"stop_name\":\"WAGON\",\"stop_sequence\":\"18\"},{\"stop_lat\":\"50.796318\",\"stop_lon\":\"4.323759\",\"stop_name\":\"EGIDE VAN OPHEM\",\"stop_sequence\":\"19\"},{\"stop_lat\":\"50.796024\",\"stop_lon\":\"4.320375\",\"stop_name\":\"CARREFOUR STALLE\",\"stop_sequence\":\"20\"},{\"stop_lat\":\"50.794795\",\"stop_lon\":\"4.319180\",\"stop_name\":\"STALLE (P)\",\"stop_sequence\":\"21\"}]}")},
		{"94809430-M20170306-000m-Semaine-00", JsonUtility.FromJson<HaltsInfoForTripRoot>("{ \"haltsInfoForTrip\" : [{\"stop_lat\":\"50.812411\",\"stop_lon\":\"4.428516\",\"stop_name\":\"HERRMANN-DEBROUX\",\"stop_sequence\":\"1\"},{\"stop_lat\":\"50.813565\",\"stop_lon\":\"4.420827\",\"stop_name\":\"DEMEY\",\"stop_sequence\":\"2\"},{\"stop_lat\":\"50.815089\",\"stop_lon\":\"4.409377\",\"stop_name\":\"BEAULIEU\",\"stop_sequence\":\"3\"},{\"stop_lat\":\"50.818543\",\"stop_lon\":\"4.404201\",\"stop_name\":\"DELTA\",\"stop_sequence\":\"4\"},{\"stop_lat\":\"50.821158\",\"stop_lon\":\"4.405451\",\"stop_name\":\"HANKAR\",\"stop_sequence\":\"5\"},{\"stop_lat\":\"50.827766\",\"stop_lon\":\"4.403881\",\"stop_name\":\"PETILLON\",\"stop_sequence\":\"6\"},{\"stop_lat\":\"50.833574\",\"stop_lon\":\"4.401685\",\"stop_name\":\"THIEFFRY\",\"stop_sequence\":\"7\"},{\"stop_lat\":\"50.839544\",\"stop_lon\":\"4.397700\",\"stop_name\":\"MERODE\",\"stop_sequence\":\"8\"},{\"stop_lat\":\"50.842783\",\"stop_lon\":\"4.381999\",\"stop_name\":\"SCHUMAN\",\"stop_sequence\":\"9\"},{\"stop_lat\":\"50.843800\",\"stop_lon\":\"4.377143\",\"stop_name\":\"MAELBEEK\",\"stop_sequence\":\"10\"},{\"stop_lat\":\"50.845454\",\"stop_lon\":\"4.369150\",\"stop_name\":\"ARTS-LOI\",\"stop_sequence\":\"11\"},{\"stop_lat\":\"50.845750\",\"stop_lon\":\"4.362434\",\"stop_name\":\"PARC\",\"stop_sequence\":\"12\"},{\"stop_lat\":\"50.846658\",\"stop_lon\":\"4.358515\",\"stop_name\":\"GARE CENTRALE\",\"stop_sequence\":\"13\"},{\"stop_lat\":\"50.850091\",\"stop_lon\":\"4.352209\",\"stop_name\":\"DE BROUCKERE\",\"stop_sequence\":\"14\"},{\"stop_lat\":\"50.852329\",\"stop_lon\":\"4.347777\",\"stop_name\":\"SAINTE-CATHERINE\",\"stop_sequence\":\"15\"},{\"stop_lat\":\"50.855060\",\"stop_lon\":\"4.340093\",\"stop_name\":\"COMTE DE FLANDRE\",\"stop_sequence\":\"16\"},{\"stop_lat\":\"50.857126\",\"stop_lon\":\"4.332011\",\"stop_name\":\"ETANGS NOIRS\",\"stop_sequence\":\"17\"},{\"stop_lat\":\"50.853041\",\"stop_lon\":\"4.322925\",\"stop_name\":\"BEEKKANT\",\"stop_sequence\":\"18\"},{\"stop_lat\":\"50.848411\",\"stop_lon\":\"4.320785\",\"stop_name\":\"GARE DE L\'OUEST\",\"stop_sequence\":\"19\"},{\"stop_lat\":\"50.843592\",\"stop_lon\":\"4.318149\",\"stop_name\":\"JACQUES BREL\",\"stop_sequence\":\"20\"},{\"stop_lat\":\"50.839535\",\"stop_lon\":\"4.312191\",\"stop_name\":\"AUMALE\",\"stop_sequence\":\"21\"},{\"stop_lat\":\"50.834856\",\"stop_lon\":\"4.305212\",\"stop_name\":\"SAINT-GUIDON\",\"stop_sequence\":\"22\"},{\"stop_lat\":\"50.829388\",\"stop_lon\":\"4.300549\",\"stop_name\":\"VEEWEYDE\",\"stop_sequence\":\"23\"},{\"stop_lat\":\"50.824972\",\"stop_lon\":\"4.297093\",\"stop_name\":\"BIZET\",\"stop_sequence\":\"24\"},{\"stop_lat\":\"50.821150\",\"stop_lon\":\"4.293877\",\"stop_name\":\"LA ROUE\",\"stop_sequence\":\"25\"},{\"stop_lat\":\"50.816032\",\"stop_lon\":\"4.290139\",\"stop_name\":\"CERIA\",\"stop_sequence\":\"26\"},{\"stop_lat\":\"50.816583\",\"stop_lon\":\"4.281255\",\"stop_name\":\"EDDY MERCKX\",\"stop_sequence\":\"27\"},{\"stop_lat\":\"50.815278\",\"stop_lon\":\"4.267408\",\"stop_name\":\"ERASME\",\"stop_sequence\":\"28\"}]}")},
		{"94809899-M20170306-000m-Semaine-00", JsonUtility.FromJson<HaltsInfoForTripRoot>("{ \"haltsInfoForTrip\" : [{\"stop_lat\":\"50.849022\",\"stop_lon\":\"4.321239\",\"stop_name\":\"GARE DE L\'OUEST\",\"stop_sequence\":\"1\"},{\"stop_lat\":\"50.853455\",\"stop_lon\":\"4.323024\",\"stop_name\":\"BEEKKANT\",\"stop_sequence\":\"2\"},{\"stop_lat\":\"50.857079\",\"stop_lon\":\"4.325279\",\"stop_name\":\"OSSEGHEM\",\"stop_sequence\":\"3\"},{\"stop_lat\":\"50.863427\",\"stop_lon\":\"4.330742\",\"stop_name\":\"SIMONIS\",\"stop_sequence\":\"4\"},{\"stop_lat\":\"50.868643\",\"stop_lon\":\"4.337002\",\"stop_name\":\"BELGICA\",\"stop_sequence\":\"5\"},{\"stop_lat\":\"50.873741\",\"stop_lon\":\"4.343165\",\"stop_name\":\"PANNENHUIS\",\"stop_sequence\":\"6\"},{\"stop_lat\":\"50.879334\",\"stop_lon\":\"4.347780\",\"stop_name\":\"BOCKSTAEL\",\"stop_sequence\":\"7\"},{\"stop_lat\":\"50.885706\",\"stop_lon\":\"4.342405\",\"stop_name\":\"STUYVENBERGH\",\"stop_sequence\":\"8\"},{\"stop_lat\":\"50.889534\",\"stop_lon\":\"4.337543\",\"stop_name\":\"HOUBA-BRUGMANN\",\"stop_sequence\":\"9\"},{\"stop_lat\":\"50.897417\",\"stop_lon\":\"4.335078\",\"stop_name\":\"HEYSEL\",\"stop_sequence\":\"10\"},{\"stop_lat\":\"50.896255\",\"stop_lon\":\"4.327062\",\"stop_name\":\"ROI BAUDOUIN\",\"stop_sequence\":\"11\"}]}")}
	};

	private static List<string> _halteNamesFromTrips;
	private List<TimeAtStationRoot> timeList;

	public static List<string> GetHaltesOnTrips()
	{
		if (_halteNamesFromTrips == null)
		{
			_halteNamesFromTrips = new List<string>();
			_halteNamesFromTrips.Clear();
			foreach (var pair in tripId_to_Positions)
			{
				if (pair.Value.haltsInfoForTrip != null)
				{
					foreach (var haltInfo in pair.Value.haltsInfoForTrip)
					{
						_halteNamesFromTrips.Add(haltInfo.stop_name.ToUpper());
					}
				}
			}
		}
		return _halteNamesFromTrips;
	}

	public bool AreConnected(string stopNameA, string stopNameB)
	{
		stopNameA = stopNameA.ToUpper();
		stopNameB = stopNameB.ToUpper();
		if (stopNameA == stopNameB) return true;
		foreach (var pair in tripId_to_Positions)
		{
			if (pair.Value.haltsInfoForTrip != null)
			{
				string lastHalte = null;
				foreach (var haltInfo in pair.Value.haltsInfoForTrip)
				{
					bool lastIsGood = lastHalte == stopNameA || lastHalte == stopNameB;
					bool nowIsGood = haltInfo.stop_name.ToUpper() == stopNameA || haltInfo.stop_name.ToUpper() == stopNameB;
					if (lastIsGood && nowIsGood) return true;
					lastHalte = haltInfo.stop_name.ToUpper();
				}
			}
		}
		return false;
	}

	public TimeAtStationRoot timeAtStation
	{
		get { return _timeAtStation; }
		set
		{
			_timeAtStation = value;

			List<MetroTrain> usedOlds = new List<MetroTrain>();
			List<VehiclePosition> usedNews = new List<VehiclePosition>();

			foreach (Transform child in transform)
			{
				var oldMetroTrain = child.GetComponent<MetroTrain>();
				foreach (var line in _timeAtStation.lines)
				{

					foreach (var vehiclePosition in line.vehiclePositions)
					{
						if (AreConnected(vehiclePosition.station, oldMetroTrain.vehiclePosition.station))
						{
							oldMetroTrain.vehiclePosition = vehiclePosition;
							usedOlds.Add(oldMetroTrain);
							usedNews.Add(vehiclePosition);
						}
					}
				}
			}

			foreach (Transform child in transform)
			{
				if (!usedOlds.Contains(child.GetComponent<MetroTrain>()))
					DestroyImmediate(child.gameObject);
			}

			foreach (var line in _timeAtStation.lines)
			{

				foreach (var vehiclePosition in line.vehiclePositions)
				{
					if (!usedNews.Contains(vehiclePosition)) // Create objects for the new data
					{
						var instance = Instantiate(this.metro_train, new Vector3(), Quaternion.identity, transform);
						instance.GetComponent<MetroTrain>().vehiclePosition = vehiclePosition;
					}
				}
			}
			FindObjectOfType<GPSViewer>().FindTargets();
		}
	}

	public void Awake()
	{
		inst = this;
		Assert.IsTrue(AreConnected("GARE DE L'OUEST", "BEEKKANT"));
		Assert.IsTrue(AreConnected("SAINTE-CATHERINE", "COMTE DE FLANDRE"));
		Assert.IsTrue(AreConnected("STOCKEL", "CRAINHEM"));
		Assert.IsFalse(AreConnected("GARE DE L'OUEST", "VANDERVELDE"));
		timeList = new List<TimeAtStationRoot>();
		for (int i = 1; i < 140; i++)
		{
			var textAsset = Resources.Load<TextAsset>(i.ToString()); //"vehiclePositions" + i.ToString("D3"));
			if (textAsset == null) continue;
			timeList.Add(JsonUtility.FromJson<TimeAtStationRoot>(textAsset.text));
		}

	}

	public static float waitTime = 0.1f;
	//private static bool _useRealtime;
	public bool useRealtime
	{
		set
		{
			Debug.Log("useRealtime "+value);
			StopCoroutine("SimulatedMetros");
			StopCoroutine("RealtimeMetros");
			if (value)
				StartCoroutine("RealtimeMetros");
			else
				StartCoroutine("SimulatedMetros");
		}
	}

	public IEnumerator Start()
	{
		yield return new WaitForEndOfFrame();

		useRealtime = false;
	}

	IEnumerator SimulatedMetros()
	{
		while (true)
		{
			foreach (var timePoint in timeList)
			{
				Debug.Log("SimulatedMetros tick");
				timeAtStation = timePoint;
				yield return new WaitForSeconds(waitTime / 3);
				yield return new WaitForSeconds(waitTime / 3);
				yield return new WaitForSeconds(waitTime / 3);
			}
			yield return new WaitForEndOfFrame();
		}
	}

	IEnumerator RealtimeMetros()
	{
		while (true)
		{
			var www = new WWW(urlTimeAtStation);
			yield return www;
			try
			{
				if (www.text == "{}" || string.IsNullOrEmpty(www.text))
				{
					Debug.Log("Ignoring '" + www.text + "' response");
				}
				else
				{
					//Debug.Log(www.text);
					Debug.Log("RealtimeMetros tick");
					timeAtStation = JsonUtility.FromJson<TimeAtStationRoot>(www.text);
				}
			}
			catch (Exception ex)
			{
				Debug.Log(www.text + "   " + ex.Message);
			}
			yield return new WaitForSeconds(21f);
		}
	}

	public void Update()
	{

	}

}
