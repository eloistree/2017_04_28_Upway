package Global;

import connexionBDD.connexionBDD;

public class GlobalVariable {

	public static connexionBDD connexionBDD = null;
	
    /**
     * Default constructor. 
     */
    private static void startBDD() {
        // TODO Auto-generated constructor stub
		connexionBDD = new connexionBDD();
		connexionBDD.connect();
		@SuppressWarnings("unused")
		int pause = 15;
    }
    
    public static connexionBDD getBDD(){
    	if(connexionBDD == null ){
    		startBDD();
    	}
    	return connexionBDD;
    }
	
}
