package servlets;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.methods.HttpGet;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import Global.GlobalVariable;



/**
 * Servlet implementation class TimeAtStation
 */
@WebServlet("/TimeAtStation")
public class TimeAtStation extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public TimeAtStation() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		try{
			String numLine = request.getParameter("line");

			if ( numLine == null){
				numLine = "1%2C5%2C6";
			}
			else{
			
				numLine = numLine.replace(",", "%2C");
			}
			System.out.println(numLine);



			String url = "https://opendata-api.stib-mivb.be/OperationMonitoring/1.0/VehiclePositionByLine/"+numLine;

			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");

			//add request header
			con.setRequestProperty("Accept", "application/json");
			con.setRequestProperty("Authorization", "Bearer 84a7680b2d1876eb0f7384d6a361751c");

			int responseCode = con.getResponseCode();
			//		System.out.println("\nSending 'GET' request to URL : " + url);
			//		System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
					new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response1 = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				String temp = inputLine;
				response1.append(temp);
			}
			in.close();

			String returning = "";

			System.out.println(response1);


			JSONParser parser = new JSONParser();

			Object obj2;
			try {
				obj2 = parser.parse(response1.toString());
				JSONObject jsonObject = (JSONObject) obj2;

				JSONArray lines = (JSONArray) jsonObject.get("lines");
				returning = returning.concat("{\"lines\":[{" );

				boolean first2 = true;

				for (Object line : lines) {
					JSONObject jsonSub = (JSONObject) line;

					if ( first2 ){
						first2 = false;
					}
					else{
						returning = returning.concat(",{" );
						}       


						String lineNumber = ((Long) jsonSub.get("lineId")).toString();

						returning = returning.concat("\"lineId\":"+lineNumber+"," );



						JSONArray vehiclePositions = (JSONArray) jsonSub.get("vehiclePositions");
						returning = returning.concat(" \"vehiclePositions\" : [{" );

						boolean first = true;
						for (Object ele : vehiclePositions) {
							if ( first ){
								first = false;
							}
							else{
								returning = returning.concat(",{" );
								}

							JSONObject jsonSub2 = (JSONObject) ele;
							Object ob = jsonSub2.get("directionId");
							String directionId = null;
							if ( ob.getClass() == java.lang.Long.class){
								directionId = ((Long) jsonSub2.get("directionId")).toString();
							}
							else{
								directionId = (String) jsonSub2.get("directionId");
							}


							String directionIdTr = GlobalVariable.getBDD().getNameOfDestination(directionId);
							if ( directionIdTr == null ){
								System.out.println(directionId);
							}
							String distanceFromPoint = null;

							if ( ob.getClass() == java.lang.Long.class){
								distanceFromPoint = ((Long) jsonSub2.get("distanceFromPoint")).toString();
							}
							else{
								distanceFromPoint = (String) jsonSub2.get("distanceFromPoint");
							}


							String pointId = null;

							if ( ob.getClass() == java.lang.Long.class){
								pointId = ((Long) jsonSub2.get("pointId")).toString();
							}
							else{
								pointId = (String) jsonSub2.get("pointId");
							}
							
							
							int numberOfDigit = 0;
							for ( int i = 0 ; i< pointId.length() ; i++ ){
								if ( Character.isDigit(pointId.charAt(i)) ){
									numberOfDigit++;
								}
								else{
									break;
								}
							}
							if ( numberOfDigit <4 ){
								switch (numberOfDigit){
								case 3 :
									pointId = "0".concat(pointId);
									break;
								
								case 2 :
									pointId = "00".concat(pointId);
									break;
								
								case 1 :
									pointId = "000".concat(pointId);
									break;
								}
							}
							
							HashMap<String,String> positions = GlobalVariable.getBDD().getPositionOfHalt(pointId);


							returning = returning.concat("\"directionId\":"+directionIdTr+"," );
							returning = returning.concat("\"distanceFromPoint\":\""+distanceFromPoint+"\"," );
							returning = returning.concat("\"long\":\""+positions.get("long").trim()+"\"," );
							returning = returning.concat("\"lat\":\""+positions.get("lat").trim()+"\"," );
							returning = returning.concat("\"station\":"+positions.get("station")+"" );


							returning = returning.concat("}" );


						}
						if ( first ){
							returning = returning.concat("}" );
						}
						returning = returning.concat("]}" );
				}


				returning = returning.concat("]}" );

				
				System.out.println(returning);


			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}






			// TODO Auto-generated method stub
			StringBuffer sb = new StringBuffer();
			sb.append(returning);
			// envoi des infos de l'en-tete
			response.setContentType("text/html");
			response.setContentLength(sb.length());
			// envoi de la réponse
			response.getOutputStream().print(sb.toString());
		}
		catch (Exception e){
			e.printStackTrace();
			StringBuffer sb = new StringBuffer();
			sb.append("{}");
			// envoi des infos de l'en-tete
			response.setContentType("text/html");
			response.setContentLength(sb.length());
			// envoi de la réponse
			response.getOutputStream().print(sb.toString());	
		}
	}

	private Object getPositionOfHalt(String pointId) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
