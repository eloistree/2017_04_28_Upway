package servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Global.GlobalVariable;

/**
 * Servlet implementation class TripInfos
 */
@WebServlet("/TripInfos")
public class TripInfos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TripInfos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		
   	String numLine = request.getParameter("tripInfo");
    	
    	String numDate = request.getParameter("date");
    	
    	if ( numLine == null){
    		numLine = "005";
    	}
    	if ( numDate == null){
    		numDate = "20170501";
    	}
		
    	 LinkedList<HashMap<String, String>> information = GlobalVariable.getBDD().getTheoricalHoraire(numDate, numLine);
    	
		 Iterator<HashMap<String, String>> it = information.iterator();
		String returning = "";
		

            returning = returning.concat("{ \"\" : [{" );
            
            boolean first = true;
            while (it.hasNext()) {
            	if ( first ){
            		first = false;
            	}
            	else{
            		returning = returning.concat(",{" );
            	}
            	
            	 HashMap<String, String> next = it.next();

            	
            	returning = returning.concat("\"route_id\":\""+next.get("route_id").replace("\"","\\\"")+"\"," );
            	returning = returning.concat("\"departure_time\":\""+next.get("departure_time")+"\"," );
            	returning = returning.concat("\"arrival_time\":\""+next.get("arrival_time")+"\"," );
            	returning = returning.concat("\"trip_headsign\":"+next.get("trip_headsign")+"," );
            	returning = returning.concat("\"trip_id\":\""+next.get("trip_id")+"\"" );
            	
            	returning = returning.concat("}" );

            	
            }
            if ( first ){
            	returning = returning.concat("}" );
            }
        
        
        returning = returning.concat("]}" );
        
       
        


		
		
		
		
		
		// TODO Auto-generated method stub
		StringBuffer sb = new StringBuffer();
		sb.append(returning);
		// envoi des infos de l'en-tete
		response.setContentType("text/html");
		response.setContentLength(sb.length());
		// envoi de la réponse
		response.getOutputStream().print(sb.toString());		
		
		
		
		
		
		
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
