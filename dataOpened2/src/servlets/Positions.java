package servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Global.GlobalVariable;

@WebServlet("/Positions")
public class Positions extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Positions() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   	String tripId = request.getParameter("trip_id");
    	

	if ( tripId == null){
		tripId = "95466486-B20170306-BRESIM01-Semaine-03";
	}	
   	
   	
    	 LinkedList<HashMap<String, String>> information = GlobalVariable.getBDD().getHaltsForTripId(tripId);
    	
		 Iterator<HashMap<String, String>> it = information.iterator();
		String returning = "";
		

            returning = returning.concat("{ \"haltsInfoForTrip\" : [{" );
            
            boolean first = true;
            while (it.hasNext()) {
            	if ( first ){
            		first = false;
            	}
            	else{
            		returning = returning.concat(",{" );
            	}
            	
            	 HashMap<String, String> next = it.next();

             	returning = returning.concat("\"stop_lat\":\""+next.get("stop_lat").trim()+"\"," );
             	returning = returning.concat("\"stop_lon\":\""+next.get("stop_lon").trim()+"\"," );
             	returning = returning.concat("\"stop_name\":"+next.get("stop_name")+"," );
             	returning = returning.concat("\"stop_sequence\":\""+next.get("stop_sequence")+"\"" );
            	
            	returning = returning.concat("}" );

            	
            }
            if ( first ){
            	returning = returning.concat("}" );
            }
        
        
        returning = returning.concat("]}" );
        
       
		
		// TODO Auto-generated method stub
		StringBuffer sb = new StringBuffer();
		sb.append(returning);
		// envoi des infos de l'en-tete
		response.setContentType("text/html");
		response.setContentLength(sb.length());
		// envoi de la réponse
		response.getOutputStream().print(sb.toString());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
}
