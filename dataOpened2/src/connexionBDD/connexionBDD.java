package connexionBDD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;




public class connexionBDD {

	private Connection connection = null;

	public boolean testDbConnexion(){
		boolean connectionNOk = false;
		
		
		
		
		if ( connection != null )
			closeBdd();
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			return connectionNOk;

		}



		try {

			connection  = DriverManager.getConnection(
					"jdbc:postgresql://127.0.0.1:5432/projSTIB", "postgres",
					"PJAC12208");

		} catch (SQLException e) {
			e.printStackTrace();
			return connectionNOk;

		}

		if (connection != null) {
				return true;

		} else {
		}
		return connectionNOk;
	}
	
	
	public boolean connect(){
		boolean connectionNOk = false;
		if ( connection != null )
			closeBdd();
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			return connectionNOk;

		}



		try {

			connection  = DriverManager.getConnection(
					"jdbc:postgresql://127.0.0.1:5432/projSTIB", "postgres",
					"PJAC12208");

		} catch (SQLException e) {
			return connectionNOk;

		}

		if (connection != null) {
				return true;
			}

		return connectionNOk;
	}	
	
	
	public void closeBdd(){
		try {
			connection.close();
		} catch (SQLException e) {
		}
	}
	


	
	public String getNameOfDestination(String idDestination){
		String returning = null;
		if( connection == null){
			System.out.println("ICI");
			testDbConnexion();
		}
		if ( connection != null){
			try {
				PreparedStatement pst = connection.prepareStatement("select \"nameDestination\" from \"transformDirectionIDintoStation\" where \"directionID\"='"+idDestination+"' ;");
				
				ResultSet rs = pst.executeQuery();
				while (rs.next()){
					returning =  rs.getString(1);
				}
				rs.close();
				pst.close();
				

			} catch (SQLException e) {
				System.out.println("ICI2");
				e.printStackTrace();
			}
			
			
		}
		else{
			System.out.println("ICI3");
			//sendMessageBddUnavailable();
		}
		return returning;		
		
	}
	
	public HashMap<String,String> getPositionOfHalt(String id){
		HashMap<String,String> returning = new HashMap<String,String>();
		if( connection == null){
			testDbConnexion();
		}
		if ( connection != null){
			try {
				PreparedStatement pst = connection.prepareStatement(""
						+ "select \"stop_lon\", \"stop_lat\", \"stop_name\"   "
						+ " from \"stops\" "
						+ "where \"stop_id\" LIKE '"+id+"%' ;");
				ResultSet rs = pst.executeQuery();
				while (rs.next()){
					returning.put("long", rs.getString(1));
					returning.put("lat", rs.getString(2));
					returning.put("station", rs.getString(3));
				}
				rs.close();
				pst.close();
				

			} catch (SQLException e) {
			}
			return returning;		
			
			
		}
		else
			System.out.println("ICI3");
			//sendMessageBddUnavailable();
		return null;
		
	}
	public LinkedList<HashMap<String,String>> getTheoricalHoraire(String date, String line ){
		LinkedList<HashMap<String,String>> returning = new LinkedList<HashMap<String,String>>();
		if( connection == null){
			testDbConnexion();
		}
		if ( connection != null){
			try {
				PreparedStatement pst = connection.prepareStatement(""
						+ "select route_id, \"departure_time\", \"arrival_time\",  trip_headsign,   trip_id  "
						+ " from \"tempoBig\" "
						+ " where \"tempoBig\".date ='"+date+"' and \"tempoBig\".route_id LIKE '"+line+"%' ; ");
				//System.out.println("select route_id, \"departure_time\", \"arrival_time\",  trip_headsign "
					//	+ " from \"tempoBig\" "
						//+ " where \"tempoBig\".date ='"+date+"' and \"tempoBig\".route_id LIKE '"+line+"%' ; ");
				ResultSet rs = pst.executeQuery();
				while (rs.next()){
					HashMap<String, String> temp = new HashMap<String,String>();
					temp.put("route_id", rs.getString(1));
					temp.put("departure_time", rs.getString(2));
					temp.put("arrival_time", rs.getString(3));
					temp.put("trip_headsign", rs.getString(4));
					temp.put("trip_id", rs.getString(5));
					returning.add(temp);
				}
				rs.close();
				pst.close();
				

			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			
		}
		else
			System.out.println("ICI3");
			//sendMessageBddUnavailable();
		return returning;		
		
	}


	public LinkedList<HashMap<String, String>> getHaltsForTripId(String tripId) {
		LinkedList<HashMap<String,String>> returning = new LinkedList<HashMap<String,String>>();
		if( connection == null){
			testDbConnexion();
		}
		if ( connection != null){
			try {
				PreparedStatement pst = connection.prepareStatement(""
						+ "SELECT  "
						+ "stops.stop_lat, "
						+ "stops.stop_lon, "
						+ "stops.stop_name, "
						+ "stop_times.stop_sequence "
						+ "FROM  "
						+ "public.stop_times, "
						+ "public.stops "
						+ "WHERE "
						+ "stop_times.trip_id = '"+tripId+"' AND "
						+ "stop_times.stop_id = stops.stop_id "
						+ "ORDER BY "
						+ "stop_times.stop_sequence::integer ASC; ");

				//System.out.println("select route_id, \"departure_time\", \"arrival_time\",  trip_headsign "
					//	+ " from \"tempoBig\" "
						//+ " where \"tempoBig\".date ='"+date+"' and \"tempoBig\".route_id LIKE '"+line+"%' ; ");
				ResultSet rs = pst.executeQuery();
				while (rs.next()){
					HashMap<String, String> temp = new HashMap<String,String>();
					temp.put("stop_lat", rs.getString(1));
					temp.put("stop_lon", rs.getString(2));
					temp.put("stop_name", rs.getString(3));
					temp.put("stop_sequence", rs.getString(4));
					returning.add(temp);
				}
				rs.close();
				pst.close();
				

			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			
		}
		else
			System.out.println("ICI3");
			//sendMessageBddUnavailable();
		return returning;	
	}
	
	
	

	
	
	
}

